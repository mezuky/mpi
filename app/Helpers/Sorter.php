<?php

namespace App\Helpers;

use App\Helpers\Sort\Bubble;
use App\Helpers\Sort\Insertion;
use App\Helpers\Sort\Merge;
use App\Helpers\Sort\Selection;
use App\Helpers\Sort\Quick;

class Sorter
{
    protected $elements = [];

    protected $sort_methods = [
        'bubble' => Bubble::class,
        'insertion' => Insertion::class,
        'merge' => Merge::class,
        'selection' => Selection::class,
        'quick' => Quick::class,
    ];

    public function sort($elements = [],  $algorithm = 'quick')
    {
        $sorter = new $this->sort_methods[$algorithm];
        return $sorter->sort($elements);
    }

    // protected function bubble()
    // {
    //     $arr = $this->elements;
    //     $size = count($arr);
    //     for ($i=0; $i<$size; $i++) {
    //         for ($j=0; $j<$size-1-$i; $j++) {
    //             if ($arr[$j+1] < $arr[$j]) {
    //                 $tmp = $arr[$j];
    //                 $arr[$j] = $arr[$j+1];
    //                 $arr[$j+1] = $tmp;
    //             }
    //         }
    //     }
    //     return $arr;
    // }

    // protected function insertion()
    // {
    //     $array = $this->elements;
    //     $length = count($array);
    //
    //     for($j=1;$j<$length;$j++) {
    //         $key = $array[$j];
    //         $i=$j-1;
    //
    //         while($i>=0 && $array[$i]>$key) {
    //             $array[$i+1]=$array[$i];
    //             $array[$i]=$key;
    //             $i--;
    //         }
    //     }
    //     return $array;
    // }

    // protected function selection()
    // {
    //     $size = count($values);
    //
    //     for($i=0;$i<=$size;$i++) {
    //        for($j=$i+1;$j<=$size;$j++) {
    //            if($values[$j] < $values[$i]) {
    //                $min = $values[$j];
    //                $values[$j] = $values[$i];
    //                $values[$i] = $min;
    //            }
    //        }
    //    }
    //
    //    return $values;
    // }

    // protected function merge()
    // {
    //     if(count($numlist) == 1 ) return $numlist;
    //
    //     $mid = count($numlist) / 2;
    //     $left = array_slice($numlist, 0, $mid);
    //     $right = array_slice($numlist, $mid);
    //
    //     $left = mergesort($left);
    //     $right = mergesort($right);
    //
    //     return merge($left, $right);
        // $data = $this->elements;
        // if(count($data)>1) {
        //
        //     // Find out the middle of the current data set and split it there to obtain to halfs
        //     $data_middle = round(count($data)/2, 0, PHP_ROUND_HALF_DOWN);
        //     // and now for some recursive magic
        //     $data_part1 = $this->merge(array_slice($data, 0, $data_middle));
        //     $data_part2 = $this->merge(array_slice($data, $data_middle, count($data)));
        //     // Setup counters so we can remember which piece of data in each half we're looking at
        //     $counter1 = $counter2 = 0;
        //     // iterate over all pieces of the currently processed array, compare size & reassemble
        //     for ($i=0; $i<count($data); $i++) {
        //         // if we're done processing one half, take the rest from the 2nd half
        //         if($counter1 == count($data_part1)) {
        //             $data[$i] = $data_part2[$counter2];
        //             ++$counter2;
        //             // if we're done with the 2nd half as well or as long as pieces in the first half are still smaller than the 2nd half
        //         } elseif (($counter2 == count($data_part2)) or ($data_part1[$counter1] < $data_part2[$counter2])) {
        //             $data[$i] = $data_part1[$counter1];
        //             ++$counter1;
        //         } else {
        //             $data[$i] = $data_part2[$counter2];
        //             ++$counter2;
        //         }
        //     }
        // }
        // return $data;
    // }

    // protected function quick()
    // {
        //
    // }
}
