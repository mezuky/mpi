<?php

namespace App\Helpers\Sort;

abstract class Sort
{
    abstract public function sort(array $items = []) : array;
}
