<?php

namespace App\Helpers;

class Generator
{
    protected $numbers = [];

    public function generate($number_of_elements = 10, $min = 0, $max = 9999)
    {
        for ($i = 0; $i < $number_of_elements; $i++) {
            $numbers[] = rand($min, $max);
        }
        return $numbers;
    }

    public function desc($number = 10, $biggest_number = 9999, $smallest_number = 0) {
        for ($i = 0; $i < $number; $i++) {
            $numbers[] = $biggest_number--;
        }
        return $numbers;
    }
}
