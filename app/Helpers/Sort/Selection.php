<?php

namespace App\Helpers\Sort;

use App\Helpers\Sort\Sort;

class Selection extends Sort
{
    public function sort(array $items = []) : array
    {
        return $this->selection($items);
    }

    protected function selection(array $items = [])
    {
        $size = count($items);

        for($i = 0; $i < $size; $i++) {
           for($j = $i+1; $j < $size; $j++) {
               if($items[$j] < $items[$i]) {
                   $min = $items[$j];
                   $items[$j] = $items[$i];
                   $items[$i] = $min;
               }
           }
       }

       return $items;
    }
}
