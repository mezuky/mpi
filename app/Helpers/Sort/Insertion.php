<?php

namespace App\Helpers\Sort;

use App\Helpers\Sort\Sort;

class Insertion extends Sort
{
    public function sort(array $items = []) : array
    {
        return $this->insertion($items);
    }

    protected function insertion(array $items = [])
    {
        $length = count($items);

        for($j = 1; $j < $length; $j++) {
            $key = $items[$j];
            $i = $j - 1;

            while ($i >= 0 && $items[$i] > $key) {
                $items[$i+1] = $items[$i];
                $items[$i] = $key;
                $i--;
            }
        }

        return $items;
    }
}
