<?php

namespace Tests\Feature\Helpers;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Helpers\Sorter;

class SorterTest extends TestCase
{
    public function test_bubble_sort()
    {
        $this->sort('bubble');
    }

    public function test_insertion_sort()
    {
        $this->sort('insertion');
    }

    public function test_selection_sort()
    {
        $this->sort('selection');
    }

    public function test_merge_sort()
    {
        $this->sort('merge');
    }

    public function test_quick_sort()
    {
        $this->sort('quick');
    }

    protected function sort($method)
    {
        $elements = [3, 104, 2, 44, 3, 9, 11, 10, 111];

        $sorter = new Sorter();
        $result = $sorter->sort($elements, $method);

        sort($elements);
        $this->assertEquals($elements, $result);
    }
}
