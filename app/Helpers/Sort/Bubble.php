<?php

namespace App\Helpers\Sort;

use App\Helpers\Sort\Sort;

class Bubble extends Sort
{
    public function sort(array $items = []) : array
    {
        return $this->bubble($items);
    }

    protected function bubble(array $items = [])
    {
        $size = count($items);

        for ($i = 0; $i < $size; $i++) {
            for ($j = 0; $j < $size-1-$i; $j++) {
                if ($items[$j + 1] < $items[$j]) {
                    $tmp = $items[$j];
                    $items[$j] = $items[$j+1];
                    $items[$j+1] = $tmp;
                }
            }
        }
        return $items;
    }
}
