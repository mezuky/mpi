<?php

namespace App\Helpers\Sort;

use App\Helpers\Sort\Sort;

class Merge extends Sort
{
    public function sort(array $items = []) : array
    {
        return $this->mergeSort($items);
    }

    protected function mergeSort(array $items = [])
    {
        if (count($items) < 2) {
            return $items;
        }

        $middle = count($items) / 2;
        $left = array_slice($items, 0, $middle);
        $right = array_slice($items, $middle);

        $left = $this->mergeSort($left);
        $right = $this->mergeSort($right);

        return $this->merge($left, $right);
    }

    protected function merge(array $left = [], array $right = [])
    {
        $result = [];
        $left_index = 0;
        $right_index = 0;

        while ($left_index < count($left) && $right_index < count($right)) {
            if ($left[$left_index] > $right[$right_index]) {
                $result[] = $right[$right_index];
                $right_index++;
            } else {
                $result[] = $left[$left_index];
                $left_index++;
            }
        }

        while($left_index < count($left)) {
            $result[] = $left[$left_index];
            $left_index++;
        }

        while($right_index < count($right)) {
            $result[] = $right[$right_index];
            $right_index++;
        }

        return $result;
    }
}
