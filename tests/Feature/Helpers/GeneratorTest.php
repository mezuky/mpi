<?php

namespace Tests\Feature\Helpers;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Helpers\Generator;

class GeneratorTest extends TestCase
{
    public function test_can_generate_array_of_numbers()
    {
        $generator = new Generator();
        $result = $generator->generate(100, 4, 955);

        $this->assertEquals(100, count($result));
        $this->assertTrue(min($result) > 3);
        $this->assertTrue(max($result) < 956);
    }
}
