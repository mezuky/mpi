<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Sorter;
use App\Helpers\Generator;
use App\Helpers\Timer;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->timer = new Timer();
        $this->sorter = new Sorter();
        $this->generator = new Generator();
    }

    public function index(Request $request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');
        $items = $this->generator->generate($request->elements);
        $method = $request->method;

        $this->timer->start();
        $this->sorter->sort($items, $method);
        $this->timer->stop();

        echo 'Sorted ' . $request->elements . ' elements with ' . $method . ' sort in ' . $this->timer->get();
    }

    public function desc(Request $request) {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');
        $items = $this->generator->desc($request->elements);

        $this->timer->start();
        $this->sorter->sort($items, $request->method);
        $this->timer->stop();

        echo 'sorted in' . $this->timer->get();
    }
}
