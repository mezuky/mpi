<?php

namespace App\Helpers\Sort;

use App\Helpers\Sort\Sort;

class Quick extends Sort
{
    public function sort(array $items = []) : array
    {
        return $this->quick($items);
    }

    protected function quick(array $items = [])
    {
        if (count($items) < 2) {
            return $items;
        }

        $left = $right = [];

        reset($items);
        $pivot_key = key($items);

        $pivot = array_shift($items);
        foreach( $items as $k => $v ) {
            if($v < $pivot) {
                $left[$k] = $v;
            } else {
                $right[$k] = $v;
            }
        }

        return array_merge($this->quick($left), [$pivot_key => $pivot], $this->quick($right));
    }
}
