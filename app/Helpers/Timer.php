<?php

namespace App\Helpers;

class Timer
{
    protected $start = 0;

    protected $stop = 0;

    public function start()
    {
        $this->start = microtime(true);
    }

    public function stop()
    {
        $this->stop = microtime(true);
    }

    public function get()
    {
        return $this->stop - $this->start;
    }
}
